<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 2018/11/1 0001
 * Time: 14:19
 */

namespace App\HttpController\Admin;
use App\HttpController\ViewController;
use App\Utility\Encrypt;

class Login extends ViewController
{
    function index()
    {
		
		if($post=$this->post()){
					
			$admin_info=osc_model('User','admin')->getByName($post['username']);
			
			if($admin_info&&$admin_info['status']==1){
				
				if(Encrypt::encode_password($post['password'],$admin_info['salt'])==$admin_info['passwd']){
					
					$login_auth = ['uid'=>$admin_info['admin_id'],
					'username'=> $admin_info['user_name'],'group_id'=>$admin_info['group_id']];	
				
					$this->session()->set('user_auth',$login_auth);
					
					$return =['success'=>'登录成功','url'=>'/admin/index/index'];
				}else{
					$return = ['error'=>'密码错误'];
				}				
				
			}else{
				$return = ['error'=>'用户不存在或被禁用'];
			}
									
			return $this->send($return);						
		}
				
				
        $this->fetch('index');
    }
	
	function logout(){
		$this->session()->set('user_auth',NULL);
		$this->response()->redirect('/admin/login/index');
	}
	
	function not_find(){		
		$this->actionNotFound('index');
	}
	
}