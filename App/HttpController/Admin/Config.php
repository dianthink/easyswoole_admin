<?php

namespace App\HttpController\Admin;
use App\HttpController\AdminController;

class Config extends AdminController
{	
	protected function _initialize() {
		
		parent::_initialize();
		$this->assign(['breadcrumb1'=>'系统']);
		$this->assign(['breadcrumb2'=>'配置管理']);		
	}

	function index(){
		
		$param=$this->get();
		
		$list=osc_model('admin','config')->get_config_page($param);		
		
		$this->fetch('index',[
			'param'=>$param,
			'module'=>osc_service('data')->get_config_module(),
			
			'empty'=>'<tr><td colspan="20">没有数据~</td></tr>',
			'list'=>$list['list'],
			'page_render'=>$list['page'],
			'total'=>$list['total']
		]);
	}	
    
	function add()
    {	
		if($post=$this->post()){
			
			$validate=osc_validate('admin','config')->validate($post);
			
			if(isset($validate['error'])){
				return $this->send($validate);
			}
			
			$r=osc_model('admin','config')->add($post);
			
			if(isset($r['error'])){
				return $this->send($r);
			}else{
				return $this->send(['success'=>'新增成功','action'=>'add']);
			}
			
			
		}
	
        $this->assign(['crumbs'=>'修改','action'=>'/admin/config/add']);
        $this->fetch('edit');
    }
	function edit()
    {
		 if($post=$this->post()){
			
			$validate=osc_validate('admin','config')->validate($post);
			
			if(isset($validate['error'])){
				return $this->send($validate);
			}
						
			if(osc_model('admin','config')->edit($post)){
												
				return $this->send(['success'=>'编辑成功','action'=>'edit']);
			}else{
				return $this->send(['error'=>'编辑失败']);
			}			
		}
		$param=$this->get();
		
		$this->assign([
			'crumbs'=>'修改',
			'action'=>'/admin/config/edit',
			'c'=>osc_model('admin','config')->get_by_id($param['id']),
			'id'=>$param['id']
		]);
        $this->fetch('edit');
    }
	function del(){
		
		if($get=$this->get()){
			
			$id=(int)$get['id'];
			
			if(osc_model('admin','config')->delete($id)){
				return $this->send(['url'=>'/admin/config/index']);					
			}			
		}
	}
}