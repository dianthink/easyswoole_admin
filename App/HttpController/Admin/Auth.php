<?php

namespace App\HttpController\Admin;
use App\HttpController\AdminController;

class Auth extends AdminController
{	
	protected function _initialize() {
		
		parent::_initialize();
		$this->assign(['breadcrumb1'=>'系统']);
		$this->assign(['breadcrumb2'=>'权限管理']);		
	}

	function index(){
		
		$param=$this->get();
		
		$list=osc_model('admin','auth')->get_auth_group_page($param);		
		
		$this->fetch('index',[
			'param'=>$param,
			'empty'=>'<tr><td colspan="20">没有数据~</td></tr>',
			'list'=>$list['list'],
			'page_render'=>$list['page'],
			'total'=>$list['total']
		]);
	}
	public function create_group(){		

		if($post=$this->post()){
			
			$validate=osc_validate('admin','auth')->create_group($post);
			
			if(isset($validate['error'])){
				return $this->send($validate);
			}
			
			$r=osc_model('admin','auth')->add_group($post);
			
			if(isset($r['error'])){
				return $this->send($r);
			}else{
				return $this->send(['success'=>'新增成功','action'=>'add']);
			}
			
			
		}
	
		$this->fetch('edit_group',[
			'breadcrumb2'=>'新增',
			'action'=>'/admin/auth/create_group',			
		]);
	}
	
	function edit_group(){
		if($post=$this->post()){
			
			$validate=osc_validate('admin','auth')->create_group($post);
			
			if(isset($validate['error'])){
				return $this->send($validate);
			}
						
			if(osc_model('admin','auth')->edit_group($post)){
												
				return $this->send(['success'=>'编辑成功','action'=>'edit']);
			}else{
				return $this->send(['error'=>'编辑失败']);
			}			
		}
		$param=$this->get();
		
		$this->assign([
			'crumbs'=>'修改',
			'action'=>'/admin/auth/edit_group',
			'group'=>osc_model('admin','auth')->get_by_id($param['id']),
			'id'=>$param['id']
		]);
        $this->fetch('edit_group');	
	}
	
	public function del_group(){
		
		if($get=$this->get()){
			
			$id=(int)$get['id'];
			
			if(osc_model('admin','auth')->delete_group($id)){
				return $this->send(['url'=>'/admin/auth/index']);					
			}			
		}
			
	}
	
	public function access(){
		
		$get=$this->get();
		
		$this->fetch('access',[
				'access_menu'=>osc_tools('tree')->list_to_tree(osc_model('admin','menu')->get_all_menu(),'id','pid','child',0),
				'rules'=>osc_model('admin','auth')->get_by_id($get['id']),
				'id'=>$get['id']
			]);
		
	}
	
	 public function write_group(){

	 	$post=$this->post();

	 	osc_model('admin','auth')->write_group($post);
	 	
	 	$this->success('编辑成功','/admin/auth/index');
	 }
	function set_status(){

    	osc_model('admin','auth')->set_status($this->get());

    	$this->response()->redirect('/admin/auth/index');	
    }
}