<?php

namespace App\HttpController;
use App\HttpController\ViewController;
use EasySwoole\EasySwoole\Config;
use think\Template;

class AdminController extends ViewController
{	
	function index(){
		
	}
	
	protected function _initialize() {
		
		$user=$this->session()->get('user_auth');
		
		if(empty($user)){	

			$this->response()->redirect('/admin/login/index');	
			
			return false;	
		}
		$this->assign(['admin_name'=>$user]);
		
		$menu=osc_model('admin','menu')->get_admin_menu($user['username'],$user['group_id']);		
		
		$this->assign(['admin_menu'=>$menu]);
		
		$server = $this->request()->getServerParams();

		$auth = new \App\Utility\Auth('admin');
		
		if($user['username']!=DbConfig('administrator')){//超级管理员不需要验证	
			if (!$auth->check(ltrim($server['request_uri'],'/'),$user['uid'])) {
						
				$this->error('没有权限');	
			}
		}
		
	}
	public function success($msg = '', $url = null){
	
		$result = [          
            'msg'  => $msg,         
            'url'  => $url,           
        ];
		
	   $view= new Template();
	   
	   $tempPath = Config::getInstance()->getConf('TEMP_DIR');
	   
	   $templatec_config=Config::getInstance()->getConf('template');
	   
	   $view->config([		
			'cache_path' => "{$tempPath}/templates_c/",               # 模板编译目录
			"tpl_replace_string"=>$templatec_config['view_replace_str'],
			'default_filter'=>''
		]);
	
	   ob_start();
       $view->fetch(EASYSWOOLE_ROOT.$templatec_config['dispatch_success_tmpl'], $result,[]);
       $content = ob_get_clean();
       $this->response()->write($content);	
	   $this->response()->end();	   
		
	}
	public function error($msg = '', $url = null){
	
		$result = [          
            'msg'  => $msg,         
            'url'  => $url,           
        ];
		
	   $view= new Template();
	   
	   $tempPath = Config::getInstance()->getConf('TEMP_DIR');
	   
	   $templatec_config=Config::getInstance()->getConf('template');
	   
	   $view->config([			
			'cache_path' => "{$tempPath}/templates_c/",               # 模板编译目录
			"tpl_replace_string"=>$templatec_config['view_replace_str'],
			'default_filter'=>''
		]);
	
	   ob_start();
       $view->fetch(EASYSWOOLE_ROOT.$templatec_config['dispatch_error_tmpl'], $result,[]);
       $content = ob_get_clean();
       $this->response()->write($content);	
	   $this->response()->end();	   
		
	}
}