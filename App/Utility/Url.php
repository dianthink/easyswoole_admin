<?php
namespace App\Utility;
class Url
{
	//拼成URL中参数的格式
	function make_url_param($param){
		$url='';
		if (!empty($param)) {
			$url= '?' . http_build_query($param, null, '&');
		}
		return $url;
	}
}