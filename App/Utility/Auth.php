<?php
namespace App\Utility;

class Auth
{	

	private $prefix;

	public function __construct($prefix)
    {
    	$this->prefix=$prefix;		
    }
	
	public function check($name,$uid)
    {	
		if(!$authList=cache('_AUTH_LIST_'.$this->prefix.'_'.$uid)){
			$authList = $this->getAuthList($uid);
			if(!empty($authList)){
				cache('_AUTH_LIST_'.$this->prefix.'_'.$uid,$authList);
			}
		}
		
     //   $authList = $this->getAuthList($uid);

      //  var_dump($authList);

		if(empty($authList)){
			return false;
		}
		
         //获取用户需要验证的所有有效规则列表
		
        if (is_string($name)) {
            $name = strtolower($name);
            if (strpos($name, ',') !== false) {
                $name = explode(',', $name);
            } else {
                $name = array($name);
            }
        }
        $list = array(); //保存验证通过的规则名
       		
        foreach ($authList as $auth) {          
             if (in_array($auth, $name)) {
                $list[] = $auth;
            }
        }
		
		if(!empty($list)) {
            return true;
        }        
        
        return false;
    }
	
    /**
     * 获得权限列表
     * @param integer $uid  用户id
     * @param integer $type
     */
    protected function getAuthList($uid)
    {
        //读取用户所属用户组
        $groups = osc_model('admin','menu')->get_auth_groups($uid,$this->prefix);
        $ids    = array(); //保存用户所属用户组设置的所有权限规则id
		
        //var_dump($groups);

		if (empty($groups)) {          
            return null;
        }
		
        foreach ($groups as $g) {
        	//dump($g);
        	if(isset($g['rules']))
            $ids = array_merge($ids, explode(',', trim($g['rules'], ',')));
        }
       // var_dump($ids);

        $ids = array_unique($ids);

        //var_dump(implode(',', $ids));

        if (empty($ids)) {          
            return null;
        }

        // 读取用户组所有权限规则
        $rules = osc_model('admin','menu')->get_auth_rule(implode(',', $ids),$this->prefix);
      //  var_dump($rules);
        // 循环规则，判断结果。
        $authList = array(); //
        foreach ($rules as $rule) {              
                $authList[] = strtolower($rule['name']);           
        }        
            //规则列表结果保存到session
       // $this->session()->set('_AUTH_LIST_'.$this->prefix.'_'.$uid,$authList);
        
        return array_unique($authList);
    }
}