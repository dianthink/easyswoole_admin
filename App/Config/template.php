<?php

return [
	
	'view_replace_str'=>[
		'__STATIC__' =>  '',
		'__ROOT__' => '',
	],
	'dispatch_error_tmpl' => '/Views/Common/error.tpl',
	//默认成功跳转对应的模板文件
	'dispatch_success_tmpl' =>  '/Views/Common/success.tpl',	
	// 异常页面的模板文件
	'exception_tmpl'         =>  '/Views/Common/exception.tpl',
	
];

?>