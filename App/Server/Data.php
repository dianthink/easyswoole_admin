<?php
namespace App\Server;
use EasySwoole\FastCache\Cache;

class Data{
	
	//取得系统配置分组列表
	function get_config_module($key=null){
		
		$module=array(
			array('module'=>'common','module_name'=>'网站公共配置'),
			array('module'=>'member','module_name'=>'会员'),
			array('module'=>'mobile','module_name'=>'移动端'),
		);
		
		foreach($module as $k => $v) {			
			$config_module[$v['module']]=$v['module_name'];			
		}
		
		if($key){
			return $config_module[$key];
		}else{
			return $module;
		}
			
	}	
	
}
?>