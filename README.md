# easyswoole_admin

#### 介绍
easyswoole简单后台。

交流请到 http://oscshop.cn/ 发表问答帖

#### 主要功能

	1.热更新
	2.系统配置管理
	3.用户权限管理
	4.菜单管理


#### 软件架构
	linux
	php7+
	mysql5.5+
	nginx
	swoole 4.3.1
	easyswoole 3.1.19-dev

#### 安装教程
	1.配置nginx
	server
    {	
		listen       80;
		server_name  1.1.1.1;
		root  /easyswoole_admin/Webroot/;
		
		location / {
			proxy_http_version 1.1;
			proxy_set_header Connection "keep-alive";
			proxy_set_header X-Real-IP $remote_addr;
			if (!-f $request_filename) {
				 proxy_pass http://127.0.0.1:9501;
			}
		}
		
    }
	
	2.导入easyswoole_admin.sql
	3.修改dev.php数据库配置
	4.php easyswoole start
	5.后台地址 xxx.com/admin456
	6.账号密码 admin 123456